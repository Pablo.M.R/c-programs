#include <stdio.h>
#include <stdlib.h>

/* El preprocesador es una herramienta de texto*/

#define EURO  166.386
#define PSTS  1 / EURO

//No se puede hacer:
// #define EURO 166.386 // Lo que vale 1 euro
//
//Si se puede hacer:
//#define EURO 166.386 /* Lo que vale 1 Euro */

#define IMPRIME       printf
#define PON_ENTERO(x) printf ("Entero: %i\n", (x));
#define SUMA2(x)      x + 2
#define SUMA(x,y)     x + y
#define SUMB(x,y)     ( (x) + (y) )
#define ESCRIBE(...)  printf ("Macro Variádica: "__VA_ARGS__)

int
main (int argc, char *argv[])
{
  IMPRIME ("El precio del euro en pesetas es: %lf\n", EURO);
  PON_ENTERO(2);
  PON_ENTERO(3);
  PON_ENTERO(SUMA2(5)); //El parametro de PON_ENTERO es: 5 + 2
  PON_ENTERO(SUMA(2, 4));

  ESCRIBE ("%i\n", 2);
  /*printf ("Macro Variádica: " "%i\n", 2);*/

  PON_ENTERO(SUMA(2, 4)*6);

  PON_ENTERO(SUMB(2, 4)*6);

  return EXIT_SUCCESS;
}
