#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{

  int n;

  printf("Dime el número de coeficientes: ");
  scanf("%i", &n);

  double a[n];
  double x;

  for (int i=0; i<=n; i++) {
    printf("Dime el valor del coeficiente a[%i]: ", i);
    scanf("%i", &i);
  }

  printf("Dime el valor de x: ");
  scanf("%d", &x);
  printf("%d", x);

  return EXIT_SUCCESS;
}
