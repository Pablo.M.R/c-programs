#include <stdio.h>
#include <stdlib.h>


#define VECES 10

void
resultado_media (double media) {
    printf ("Media: %.2lf\n", media);
}

int
main (int argc, char *argv[])
{
    double entrada,
           suma = 0;

    for (unsigned i=0; i<VECES; i++){
        printf ("Número %u: ", i+1);
        if (scanf (" %lf", &entrada) == 0)
        suma += entrada;
    }


    resultado_media ( suma / VECES );

    return EXIT_SUCCESS;
}
