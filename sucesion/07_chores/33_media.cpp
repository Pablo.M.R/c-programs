#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x100



/* devuelve true si se ha podido leer el número correctamente. */
bool
numero (double *numero /* Paso por referencia */) {
    char entero[MAX],   // Quiero leer parte entera, decimal o las dos
         decimal[MAX],
         entrada[2*MAX];
    int partes_leidas = 0;


    printf ("Número: ");

    if (scanf ("%[0-9]", entero))
        partes_leidas++;

    if (scanf (".%[0-9]", decimal))
        partes_leidas++;

    if (partes_leidas == 0)
        return false;

    strcpy (entrada, entero);
    strcpy (entrada, ".");
    strcpy (entrada, decimal);


    /* Convertir una cadena de caracteres a número real */
    *numero = atof (entrada);
    return true;
}




void
imprimir_resultado (double r) {
    printf ("Media: %.2lf\n", r);
    printf ("\n");
}


int
main (int argc, char *argv[])
{
    double suma = 0.,
           nuevo,
           media;
    unsigned cuenta = 0;;

    /* ENTRADA DE DATOS */
    while ( numero (&nuevo) ){
        suma += nuevo;
        cuenta++;
    }

    /* CÁLCULOS */
    media = suma / cuenta;

    /* SALIDA DE DATOS */
    imprimir_resultado (media);

    return EXIT_SUCCESS;
}
