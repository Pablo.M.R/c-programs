#include <stdio.h>
#include <stdlib.h>


#define VECES 10  /* Número de números que se van a preguntar */

void
error ()
{
    fprintf (stderr, "Has metido un número mal.\n");
    exit (EXIT_FAILURE);
}

void
ver_media (double media) {
    printf ("===============\n"
            "  Media: %.2lf\n"
            "===============\n"
            , media);
    printf ("\n");
}

int
main (int argc, char *argv[])
{
    double entrada,
           suma = 0;

    for (unsigned i=0; i<VECES; i++){
        printf ("Número %u: ", i+1);
        if (scanf (" %lf", &entrada) == 0)
            error ();
        suma += entrada;
    }


    ver_media ( suma / VECES );

    return EXIT_SUCCESS;
}
