#include <stdio.h>
#include <stdlib.h>

int suma (int op1,int op2) {
   return op1 + op2;
}

int resta (int op3, int op4) {
    return op3 - op4;
}

int multi (int op6,int op7) {
    return op6 * op7;
}

int divi (int op8, int op9) {
    return op8 / op9;
}

void incre (int *op10, int op11) {
    *op10 += op11;
}

int
main (int argc, char *argv[])
{
  int a = 6;
  int b = 2;
  int sum;
  int rest;
  int mult;
  int div;

  sum = suma(a, b);
  rest = resta(a, b);
  mult = multi(a, b);
  div = divi(a, b);

  printf("El resultado de la suma es %i\n", sum);
  printf("El resultado de la resta es %i\n", rest);
  printf("El resultado de la multiplicacion es %i\n", mult);
  printf("El resultado de la división es %i\n", div);

  incre (&a, 5);
  printf("El incremento de a = %i\n", a);

  return EXIT_SUCCESS;
}
