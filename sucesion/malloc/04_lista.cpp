#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

int
main (int argc, char *argv[])
{
    char **lista;
    int num;

    printf("¿Cuantas palabras quieres?: ");
    scanf("%i", &num);
    lista = (char **) malloc ( num * sizeof(char *) );
    bzero ((void *) lista, sizeof (lista));

    printf("\n");

    for (int i=0; i<num; i++){
        printf("Dime la %i palabra: ", i+1);
        scanf("%ms", &lista[i]);
    }

    printf("\n");

    for (int i=0; i<num; i++)
        printf("Palabra %i: %s\n", i+1, lista[i]);

  return EXIT_SUCCESS;
}
