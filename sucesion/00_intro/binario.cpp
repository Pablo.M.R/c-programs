#include <stdio.h>

int main() {

    int op1,
        op2;
    printf("Dime el primer operador: ");
    scanf("%i", &op1);

    printf("Dime el segundo operador: ");
    scanf("%i", &op2);

    printf("%i XOR %i = %i\n", op1, op2, op1 ^ op2);

    return 0;
}

