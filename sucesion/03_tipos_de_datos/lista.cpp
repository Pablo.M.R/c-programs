#include <stdio.h>
#include <stdlib.h>

#define N 4
#define ROW 5
#define COL 4

int main () {

    int sueldo0,
        sueldo1,
        sueldo2,
        sueldo3;

    int sueldo[N] = { 900, 1900, 2999, 1457 };

    int *sueldo_max;

    int tablero[ROW][COL] = {
        {1, 0, 2, 4},
        {1, 0, 2, 4},
        {1, 0, 2, 4},
        {1, 0, 2, 4},
        {1, 0, 2, 4},
    };

    sueldo[0] = sueldo[1] = sueldo[2] = sueldo[3] = 1400;
    tablero[0][3] = 5;

    sueldo == &sueldo[0];

    sueldo_max = &sueldo[1];

    printf ("%lu\n", sizeof (sueldo));
    printf ("%lu\n", sizeof (sueldo_max));

    return EXIT_SUCCESS;
}
