var WIDTH
var HEIGHT
var XC
var YC

var R = 200

var vertex = [
    [ R*.866,  R/2],
    [ 0     ,  R],
    [-R*.866,  R/2],
    [-R*.866, -R/2],
    [ 0     , -R],
    [ R*.866, -R/2]
]

function init () {
    WIDTH  = parseInt (canvas.dataset.width)
    HEIGHT = parseInt (canvas.dataset.height)
    XC = WIDTH  / 2
    YC = HEIGHT / 2
}

function x (xl) { return XC + xl }
function y (yl) { return YC - yl }

function draw (ctx, vertex) {
    ctx.moveTo (x (vertex[0][0]), y (vertex[0][1]))
    for (var i=1; i<vertex.length; i++)
        ctx.lineTo (x (vertex[i][0]), y (vertex[i][1]))
    ctx.lineTo (x (vertex[0][0]), y (vertex[0][1]))

    ctx.stroke ()
}

function rotate(value) {
  document.getElementById('output').style.webkitTransform="rotate(" + value + "deg)";
  document.getElementById('output').style.msTransform="rotate(" + value + "deg)";
  document.getElementById('output').style.MozTransform="rotate(" + value + "deg)";
  document.getElementById('output').style.OTransform="rotate(" + value + "deg)";
  document.getElementById('output').style.transform="rotate(" + value + "deg)";
  document.getElementById('valor').innerHTML=value + "deg";
}

function main () {
    var slider = document.getElementById("angle")
    var output = document.getElementById("valor")

    output.innerHTML = slider.value

    slider.oninput = function (){
          output.innerHTML = this.value
      }

    var canvas = document.getElementById("canvas")
    var ctx    = canvas.getContext("2d")

    init ()
    draw (ctx, vertex)

    let angle = slider * Math.PI / 180

    var R = [

        [Math.cos (angle), -Math.sin (angle)],
        [Math.sin (angle),  Math.cos (angle)]
    ]

    var rotated = []
    for (var i=0; i<vertex.length; i++)
        rotated[i] = [
            vertex[i][0] * R[0][0] + vertex[i][1] * R[0][1],
            vertex[i][0] * R[1][0] + vertex[i][1] * R[1][1]
        ]

    draw (ctx, rotated)

}
