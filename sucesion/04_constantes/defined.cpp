#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{

  printf ("¿Hola como estas?\n");
  printf ("Esto es el fichero " __FILE__ "\n");
  printf ("Estas en la linea %i\n", __LINE__);
  printf ("Esto es el fichero %s\n", __FILE__);

  return EXIT_SUCCESS;
}
