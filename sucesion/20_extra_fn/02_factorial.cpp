#include <stdio.h>
#include <stdlib.h>

unsigned
factorial(unsigned num) {
    if (num == 0)
        return 1;
    return num * factorial (num - 1);
}

int
main (int argc, char *argv[])
{
    unsigned num;

    printf("Factorial\n");
    printf("\nNumero: ");
    scanf(" %u", &num);

    printf("El factorial de %u es %u\n", num, factorial (num));

  return EXIT_SUCCESS;
}
