#include <stdio.h>
#include <stdlib.h>

int main()
{
  FILE *cs;
  char caracter;

  cs = fopen("texto.txt","r");

  if (cs == NULL)
    {
      printf("\nError de apertura del archivo. \n\n");
    }
    else
    {
      printf("\nEL contenido del archivo es: \n\n");
        while((caracter = fgetc(cs)) != EOF)
          {
	    printf("%c",caracter);
	  }
    }
  fclose(cs);
  return EXIT_SUCCESS;
}
