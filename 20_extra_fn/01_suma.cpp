#include <stdio.h>
#include <stdlib.h>

unsigned
suma (unsigned n) {
    if (n == 0)
        return 0;
    return n + suma (n - 1);
}

int
main (int argc, char *argv[])
{
    unsigned n;

    printf("SUMA DE GAUSS\n");
    printf("\nNumero: ");
    scanf(" %u", &n);

    printf("S(%u) = %u\n", n, suma (n));

  return EXIT_SUCCESS;
}
