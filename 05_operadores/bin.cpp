#include <stdio.h>
#include <stdlib.h>

void
metodo1 (unsigned decimal)
{
  while (decimal > 0) {
    printf ("%u", decimal % 2);
    decimal >>= 1;
  }
  printf ("\n");
}

void
metodo2 (unsigned decimal)
{
  for ( ;decimal > 0; decimal >>= 1)
      printf ("%u", decimal % 2);
  printf ("\n");
}




int
main (int argc, char *argv[])
{

unsigned decimal;

  scanf (" %u", &decimal);
  printf (" %u\n", decimal);

  metodo2 (decimal);

  printf ("\n");

  return EXIT_SUCCESS;
}
