#include <stdio.h>
#include <stdlib.h>

int
le_de_la_p_gana_al_usuario (int s, int *room)
{
    return  room[s - 1] < 0 ? 0 : 1;
}

int
main (int argc, char *argv[])
{

    int *room = NULL;
    int summit = 0;

    do {
        room = (int *) realloc ( room, (summit + 1) * sizeof (int) );
        printf ("Número: ");
        scanf  ("%i", &room[summit++]);
    } while (le_de_la_p_gana_al_usuario (summit, room));
    summit--;

    printf ("\n");
    for (int i=0; i<summit; i++)
        printf ("%i: %i\n", i+1, room[i]);
    printf ("\n");

    free (room);

    return EXIT_SUCCESS;
}

