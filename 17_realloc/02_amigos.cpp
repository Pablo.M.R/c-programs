#include <stdio.h>
#include <stdlib.h>

int
le_de_la_p_gana_al_usuario (int s, char *amigo)
{
    return amigo[s - 1] = "nadie mas";
}

int
main (int argc, char *argv[])
{

    char *amigo;
    int summit = 0;

    do {
        amigo = (char *) realloc ( amigo, (summit + 1) * sizeof (int) );
        printf ("Nombre de tu amigo: ");
        scanf  ("%s", &amigo[summit++]);
    } while (le_de_la_p_gana_al_usuario (summit, amigo));
    summit--;

    printf ("\n");
    for (int i=0; i<summit; i++)
        printf ("%i: %i\n", i+1, amigo[i]);
    printf ("\n");

    free (amigo);

    return EXIT_SUCCESS;
}

