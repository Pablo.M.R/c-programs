#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    char **amigos = NULL;
    char **p;
    int input = 0;
    int nnombres = 0;

    do {
        system("clear");
        amigos = (char **) realloc (amigos, (nnombres + 1) * sizeof (char *));
        printf ("Dime como se llama tu amigo: ");
        input = scanf ("%m[a-z A-Z]", amigos + nnombres++);
        __fpurge (stdin);
    } while (input);

    system ("clear");
    p = amigos;
    while (*p != NULL) {
        printf ("%s\n", *p);
        p++;
    }

    for (char **p=amigos; *p!=NULL; p++)
        free (*p);
    free (amigos);

  return EXIT_SUCCESS;
}
