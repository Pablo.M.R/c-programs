
                  // Esto es un comentario de una linea
                  /* Esto es un comentario de varias lineas */

int a;            // Variable global
                  // Declarada fuera de todas las funciones.
                  // Se puede usar desde esta línea en adelante.
                  // No se deben usar de manera habitual.
                  // VB por defecto = 0

void              // No devuelve nada: void = vacío
haz_algo ()       // Usamos la notación underscore
                  // En camelcase: hazAlgo (no usar)

{
  return;         // Cuando se ejecuta vuelve al lugar
                  // desde el que le han llamado.
                  // Puede devolver un valor o no.
                  // Es innecesario en las funciones void.
                  // Cuando una función void llega al final
                  // ejecuta un return por si solo.
}

int               // main devuelve un entero
main ()           // Declara la funcion main
{
  int c;          // Variable local.
                  // Se define dentro de {}
                  // Sólo se conocen en la función.
                  // Al terminar la función su valor desaparece.
                  // Por defecto ? (rubbish)


  2 + a;          // Las instrucciones terminan en ";"
                  // Esto es una expresión.
                  // Todo tiene un Valor de Retorno, pero...
                  // ¿Qué estamos haciendo con le VR?

  return 0;       // Devuelve un valor a la funión que llamó.
                  // main => bash
                  // 0 => todo ok
                  // != 0 => wrong
}
