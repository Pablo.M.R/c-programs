#include <stdio.h>
#include <stdlib.h>

int main () {

    printf ("sizeof (int) == %lu\n", sizeof (int));

    for (unsigned char i=0x20; i<0x80; i++)
        printf("%Xh: %c\n", i, i);

    return EXIT_SUCCESS;
}
