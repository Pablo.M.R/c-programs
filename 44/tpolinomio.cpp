#include <iostream>
#include <cmath>

using namespace std;

double evalua(double a[], int n, double x); // prototipo

int main()
{
   int n;   // # de coeficientes

   cout << "Ingrese el # de coeficientes n = ";
   cin >> n;

   double a[n]; // Arreglo de coeficientes
   double x;

   for (int i = 0; i < n; i++) {
      cout << "Coeficiente a[" << i << "] = ";
      cin >> a[i];
   }

   cout << "Ingrese un valor para x = ";
   cin >> x;

   double valor_numerico = evalua(a, n, x);
   cout << "\nValor numerico p(" << x << ") = " << valor_numerico << endl;

   cin.get();
   cin.get();
   return 0;
}

double evalua(double a[], int n, double x) {
   // Cálculo del valor numérico de P(x)
   double p = 0.0;
   for (int i = 0; i < n; i++) {
      p = p + a[i] * pow(x, i);
   }

   return p;
}
